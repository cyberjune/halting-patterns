---
title: "Shell and Brain"
date: 2020-02-10T10:00:00Z
author: "Shane Kilkelly"
---


In October I read Ross Ashby's "Introduction to Cybernetics" while on a trip
to the States, finding myself with free time to read for the first time in
months. It's taken a while to get back around to writing up my analysis of the
book, but that's just what it is to be interested in this stuff in a capitalist
economy. Let's start with one of the more interesting chapters in the book,
Chapter 10: Regulation in Biological Systems.


## Life and Survival

> The chief purpose of this chapter is to tie together the concepts of
> regulation, information, and survival, to show how intimately they are
> related, and to show how all three can be treated by a method that is entirely
> uniform with what has gone before in the boot, and that can be made as
> rigorous, objective, and unambiguous as on pleases.
> 
> - Ross Ashby, Introduction to Cybernetics, 10/2

At this late point in the book, Ashby has taught us about machines, systems of
machines, and the variety of their behaviours and configurations. We get the
sense that he's now finally getting to the point of what he wanted to teach us
all along, the application of this theory to the regulation of lively systems in
the world, and to the understanding of how that regulation is to be achieved.

Ashby sets up the chapter by talking about evolution and survival, the way in
which life tends to propagate and change through time. Survival means
maintaining some kind of integrity and continuity in the living system, both at
the level of the individual and at the level of the species, that bizarre
multi-scale process which is working itself out across time and space. The gene
and the individual are mutually entangled in a process of becoming, with each
contributing to the survival of the other, and itself, and the overall process.
Ashby introduces the contrast between shells and brains as mechanisms of
survival:

> What this means is that those gene-patterns are specially likely to survive
> (and therefore to exist today) that cause to grow, between themselves and the
> dangerous world, some more or less elaborate mechanism for defence. So the
> genes in _Testudo_ cause the growth of a _shell_; and the genes in _Homo_
> cause the growth of a _brain_. (The genes that did not cause such growths have
> long since been eliminated)
>
> - Ross Ashby, Introduction to Cybernetics, 10/5


## The Dangerous World

At this point Ashby injects a now-familiar diagram, having developed the theory
throughout the book:

![D-F-E Diagram](/img/ashby-dfe.png)

In this diagram, `"D"` is the source of disturbance and danger originating from
the environment. `"E"` is the essential variables of the system in question (the
organism), and `"F"` is "the interpolated part (shell, brain, etc) formed by the
gene-pattern for the protection of E". In this diagram, "F" is a regulator,
trying to keep "E" stable in the presence of "D".

This is the same setup as Ashby's earlier example of a temperature-controlled
water bath, in which a thermostat regulates the temperature of the water. Ashby
is framing survival in terms of homeostatic regulation, in other words, in terms
of a machine-like process which can be analysed, understood, and even designed.

Here is a table showing the similarities between the water bath and the organism:

|   | Water Bath  | Organism     |
|---|-------------|--------------|
| D | Weather     | Danger       |
| F | Thermostat  | Shell/Brain  |
| E | Temperature | Blood Volume |


## Regulation Blocks Information

It now becomes clear why Ashby spent almost two-hundred pages on elaborating a
theory of information propagation in machinic systems. The survival of the
organism depends on protecting some set of "essential variables" (such as its
blood volume, or simply its "aliveness") from a theoretically infinite set of
possible disturbances, such as misadventure, severe weather, violence from other
agents in the world, loss of access to food, and so on.

The essential variables must be held (relatively) steady, regardless of what
happens out there in the world. Or, another way of putting it; the organism
itself would prefer to control _how_ its own variables change over time. It
requires some kind of regulator (or set of regulators) to do this.

This is a problem of variety and information, the environment and all its
dangers are high-variety, while the space of preferred states for the organism
is relatively low-variety. If the regulator is to be effective, it must block
the variety of the world from impinging on the variety of its essential
variables. It must block the flow of information which would otherwise flow from
"D" to "E". If "D" were to propagate information to "E", then the organism would
effectively not exist, it would instead just be a pile of matter blowing about
in the wind, with nothing to distinguish itself from any other pile of matter in
the environment.

Ashby calls back to an earlier chapter in which he established that a
properly-functioning thermostat blocks information from the outside from
affecting the temperature of the water in a bath or tank. If one were to sit all
day and stare at a temperature read-out from the water tank, one would receive
no information about changes going on outside, there would just be a flat line
held at a constant temperature. By contrast, a malfunctioning thermostat would
fail to stabilize the temperature of the water, and thus would allow information
to creep in from outside. The observer could infer from the fluctuation in
temperature that a sun-beam was shining on the water tank, or that someone had
left a door open and allowed a cold breeze to blow around the tank.

![Diagram of information blocking with thermostat](/img/ashby-info-block.png)

The same principle applies to living systems, and to the higher activities of
the more complex living systems. Come rain or shine, they must keep themselves
alive, to protect their essential variables from the explosive cascading
information flows of the outside. Their "aliveness" (and its composite
variables) must remain steady. And given that the world produces seemingly
infinite variety of danger, they need _excellent_ regulators to achieve this
dynamic stability


## Brain as (Superior) Alternative to Shell

And so we're back to shells and brains. I'll just quote what Ashby has to say on
this:

> The blocking may take place in a variety of ways, which prove, however, on
> closer examination to be fundamentally the same. Two extreme forms will
> illustrate the range.
> 
> One way of blocking the flow (from the source of disturbance D to the
> essential variable E) is to interpose something that acts as a simple passive
> block to the disturbances. Such is the tortoise’s shell, which reduces a
> variety of impacts, blows, bites, etc. to a negligible disturbance of the
> sensitive tissues within. In the same class are the tree’s bark, the seal’s
> coat of blubber, and the human skull.
> 
> At the other extreme from this static defence is the defence by skilled
> counter-action—the defence that gets information about the disturbance to
> come, prepares for its arrival, and then meets the disturbance, which may be
> complex and mobile, with a defence that is equally complex and mobile. This is
> the defence of the fencer, in some deadly duel, who wears no armour and who
> trusts to his skill in parrying. This is the defence used mostly by the higher
> organisms, who have developed a nervous system pre- cisely for the carrying
> out of this method.
> 
> When considering this second form we should be careful to notice the part
> played by information and variety in the process. The fencer must watch his
> opponent closely, and he must gain information in all ways possible if he is
> to survive. For this pur- pose he is born with eyes, and for this purpose he
> learns how to use them. Nevertheless, the end result of this skill, if
> successful, is shown by his essential variables, such as his blood-volume,
> remaining within normal limits, much as if the duel had not occurred.
> Information flows freely to the non-essential variables, but the variety in
> the distinction “duel or no-duel” has been pre- vented from reaching the
> essential variables.
>
> - Ross Ashby, Introduction to Cybernetics, 10/7

What I find interesting here is the way Ashby frames the brain as a superior
alternative to a shell. We usually think of the brain as being encased inside
the body, a weak, squishy organ which needs a body to protect it from the
outside world. But the framing here is quite the opposite: _the body needs a
brain to protect it from the outside world_. It's as if the brain were wrapped
around the body as a shell, interfacing with the world on behalf of the bodys
essential variables, acting as a pro-active dynamic defense system. The body is
the cargo, while the brain is the hull, the rudder, the wheel, and the
navigator.

This turns the modern folk-understanding of the brain/body system on its head.
Rather than a precious cognitive cargo being shielded from the elements by a
squishy body, instead the body is protected from the sheer weirdness of the
world by a robust and dynamic nervous system with the capacity to learn, adapt,
and to model its past, present, and future. The radical openness and dynamism of
the brain vastly outperforms and outmaneuvers the crude defenses of the shell.
Just ask any turtle who's gone toe-to-toe with a fencer.

Fools lament the lack of a shell on the human back. What need have we for a
shell when we have something so much more powerful mediating our engagement with
the world?


## Social Shells and Social Brains

Cybernetics posits that there are general principles underlying the behaviour
and development of dynamic, adaptive systems. The lessons we learn from biology
can be condensed and abstracted, and then applied to the social world.

Plenty of social thinkers have used the shell metaphor (or some variant of it),
to defend and naturalize the worst aspects of human geopolitics. The liberal
order of nation states and the patchwork fever-dreams of the neo-reactionaries
are both laced through with notions of borders, walls, blockages, islands,
enclaves, pockets, defenses, fortifications, protection, isolation. The
nation-state is the psycho-sexual anus-clenching reflex made material in the
world. A socio-psychological derangement written into law. Leviathan, with its
skin of armoured scales, squats over the land, pretending to keep order, and
pretending to keep that order on behalf of the people.

And this system of states is failing miserably. The crisis cascades and
multiplies. Despite the obvious uselessness of this method of social
organisation it seems to just be what we're stuck with for the moment. The
capitalist state (and world) system clenches harder, tries to harden its shells,
and forbids the development of real alternatives to its failure.

Even the
left (whatever remains of it) struggles to move beyond shell-thinking when
trying to articulate a vision of a better future. 

Many morbid symptoms emerge from this lack of imagination. On the right and
center of political discourse, people either advocate for stronger shells and
harder borders, or for an incoherent mix of slightly weaker shells but without
any kind of social brain to replace the shell, a world of authoritarian
market-states (in the "small state" sense so loved by libertarians and
centrists) dissolved in the acid-bath of capital, lacking any higher brain
function. What a grim set of choices! Either a patchwork amoeba of hard-bordered
ethnostates or a patchwork amoeba of bueraucratic market domination. 

On the left we're not much better. We either rally behind whatever state
electoral project is being offered and implicitely tie our flag to one of the
afformentioned masts, or we fall back on old articles of faith like Leninism, or
so many dead-eyed variants on Trotskyism. Shell-thinking, all of it.

In the few instances where we do articulate an anti-state/anti-shell politics we
get stuck in one of two traps. In both cases the alternative is (assumed to be)
worse than the state, and we only differ in our attitude toward this worseness.
Either the worse alternative is unacceptable, in which case we fall back to a
depressive reformism within the state, or the worse alternative is still
preferable despite its nightmarish implications, so we just have to all get
accustomed to being worse off just because. The juice is worth the squeeze,
supposedly.

Missing from almost all of this is any kind of brain-thinking, the discovery and
development of social brains more powerful, more effective, more open, more free
than social shells. Marx gave us a start, Beer planted some way-markers, others
fleshed out the map as best the could. We're still only beginning to grapple
with this problem.

We need to imagine our collective future as not simply another State/Shell arrangement
where we pretend that imagined boundaries are sufficient to achieve long-term
dynamic stability and flourishing for life on earth. We need to think instead of
what a Social Brain would look like. Open and dynamic, always moving and
evolving, always learning, always ready and willing to abandon the sad tyranny
of the past and to navigate the future. And beyond imagining, we need to _act_
in such a way as to bring about the emergence of this common social brain. This
will be a revolution.

Fools lament the lack of a social shell for human society. What need have we for
a shell when we can have something so much more powerful mediating our
development within the world?
